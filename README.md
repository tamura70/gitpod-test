# README

## 最初の利用 (学生用)

1. <https://gitlab.com> にアカウントがなければ，アカウントを作成する．
2. <https://gitpod.io/#https://gitlab.com/tamura70/gitpod-test> を開き，GitLabのアカウントでログインする．
3. しばらく待つ．
4. 画面下側のターミナルウィンドウに以下を入力すると，"Hello Kobe!"が表示されるはず．
   ```
   make
   ./hellokobe
   ```


2回目以降は <https://gitpod.io> を開き，GitLabのアカウントでログインし，
gitpod-testのworkspaceをOpenあるいはStartする．

### 利用上の注意

- 50時間/月までは無料
- IDEはVisual Studio Code互換
- クラウド上でLinuxコンテナが起動されており，
  そのターミナル画面が画面下部に表示されている．
  ターミナル画面でプログラムのコンパイル・実行が可能
- `.gitpod.Dockerfile` の設定で，プログラムのインストールなどが可能
- ファイルの編集内容は，2週間以内に再度workspaceを開けば残っている．
- workspaceを保存してダウンロードすることも可能
- その他の詳しい使い方は Gitpod の Help や以下を参照のこと．
   - <https://kobeucsenshu.github.io/ideinfo/> (システム情報学研究科鎌田先生作成)

## 授業用のgitリポジトリの作成 (教師用)

作成中
